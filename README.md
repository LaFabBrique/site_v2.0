# Site web La Fab'Brique V2.0

Fait suite à la V1.0 réalisé sous Wordpress.

_Statut: WIP_

Customisation d'un [Bootstrap](http://getbootstrap.com/) + thème [Cardio](http://tympanus.net/codrops/?p=24301), par [PHIr](http://www.phir.co/).

----
## www.lafabbrique.org


### License

Les pages de se site sont publiées sous licence Creative Commons - Attribution 2.0-Fr ([CC-BY](https://creativecommons.org/licenses/by/2.0/fr/))

Les crédits et licences des fichiers utilisés par le thème sont détaillés dans le [fichier README original](/README_orgn.md)
